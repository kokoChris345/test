import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import * as courseActions from  '../../actions/courseAction';
import {bindActionCreators} from 'redux';
import CourseList from './CourseList';
import {browserHistory} from 'react-router';

class CoursesPage extends React.Component {
    constructor(props,context) {
        super(props,context);
        this.redirectToAddCoursePage = this.redirectToAddCoursePage.bind();


    }

    redirectToAddCoursePage () {
        browserHistory.push('/course');
    }
    render() {
        const { courses } = this.props;
        return (
            <div>
                <h1>Courses</h1>
                <input type="Submit"
                       value="Add Course"
                       className="btn btn-primary"
                       onClick = {this.redirectToAddCoursePage}
                />
               <CourseList courses={courses} />
            </div>

        );
    }
}

CoursesPage.propTypes = {
  courses:  PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired
};
function mapStateToProps(state, ownProps) {
    return {
      courses: state.courses
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(courseActions, dispatch)
    };
}

export default connect(mapStateToProps,mapDispatchToProps)(CoursesPage);